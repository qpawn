#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>

// redfefine as bitfield
enum button {
        A, B, X, Y, U, D, L, R, ST, SE, SL, SR };
enum btnstate {
        UP, DOWN };
struct event {
        enum btnstate state;
        enum button btn; };

// FFI these
void   quit();
void   pixel(int, int);
void   color(int, int, int);
void   clear();
void   draw();
void   sleep(int);
struct event* events();

void   line(int, int, int, int);
void   rect(int, int, int, int);


static SDL_Window*   window   = 0;
static SDL_Renderer* renderer = 0;

void init()
{
        if (SDL_Init(SDL_INIT_VIDEO) != 0) {
                printf("sdl init failed");
                quit();
                return;
        }
        if (SDL_CreateWindowAndRenderer(640,480,0,&window,&renderer)!=0){
                printf("create window&render failed");
                quit();
                return;
        }
}

void quit()
{
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
}

void clear()
{
        SDL_RenderClear(renderer);
}

void color(int r, int g, int b)
{
        SDL_SetRenderDrawColor(renderer, r, g, b, 255);
}

void pixel(int x, int y)
{
        SDL_RenderDrawPoint(renderer, x, y);
}

void draw()
{
        SDL_RenderPresent(renderer);
}

void sleep(int t)
{
        SDL_Delay(t * 1000);
}

void inline line(int xa, int ya, int xb, int yb)
{
        SDL_RenderDrawLine(renderer, xa, ya, xb, yb);
}
        
void inline rect(int xa, int ya, int xb, int yb)
{
        SDL_Rect r;
        r.x = xa; r.y = ya; r.w = xb-xa; r.h = yb-ya;
        SDL_RenderFillRect(renderer, &r);
}

