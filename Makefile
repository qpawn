all: qpawn.o QPawn.hs
	ghc -Wall -I/usr/include/SDL2 -lSDL2 qpawn.o QPawn.hs -o qpwn

qpawn.o: qpawn.c
	gcc -Wall -I/usr/include/SDL2 -lSDL2 -c qpawn.c

clean:
	rm qpwn *.o *.hi *.hc
