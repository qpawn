{-# LANGUAGE ForeignFunctionInterface #-}

module Main where

import Foreign
import Foreign.C.Types
import Data.Int

-- | foreign imports
foreign import ccall "qpawn.c init"
    c_init :: IO ()

foreign import ccall "qpawn.c quit"
    c_quit :: IO ()

foreign import ccall "qpawn.c clear"
    c_clear :: IO ()

foreign import ccall "qpawn.c draw"
    c_draw :: IO ()

foreign import ccall "qpawn.c pixel"
    c_pixel :: CInt -> CInt -> IO ()

foreign import ccall "qpawn.c color"
    c_color :: CInt -> CInt -> CInt -> IO ()

foreign import ccall "qpawn.c sleep"
    c_sleep :: CInt -> IO ()

foreign import ccall "qpawn.c line"
    c_line :: CInt -> CInt -> CInt -> CInt ->  IO ()

foreign import ccall "qpawn.c rect"
    c_rect :: CInt -> CInt -> CInt -> CInt ->  IO ()

-- | actual code
ap2 :: (a -> a -> b) -> [a] -> b
ap2 f (a:b:_) = f a b
ap2 _ _       = error "a list too small given to ap2"

ap3 :: (a -> a -> a -> b) -> [a] -> b
ap3 f (a:b:c:_) = f a b c 
ap3 _ _         = error "a list too small given to ap3"

ap4 :: (a -> a -> a -> a -> b) -> [a] -> b
ap4 f (a:b:c:d:_) = f a b c d
ap4 _ _           = error "a list too small given to ap4"

cint :: (Integral a) => a -> CInt
cint n = (fromIntegral n) :: CInt

cints :: (Integral a) => [a] -> [CInt]
cints = map cint

color :: [Int] ->  IO ()
color = ap3 c_color . cints

pixel :: [Int] -> IO ()
pixel = ap2 c_pixel . cints

line :: [Int] -> IO ()
line = ap4 c_line . cints

rect :: [Int] -> IO ()
rect = ap4 c_rect . cints

clear :: IO ()
clear = c_clear

sleep :: Int -> IO ()
sleep = c_sleep . cint

draw :: IO ()
draw = c_draw

main :: IO ()
main = do c_init
          color [0,0,0]
          clear
          draw
          sleep 2
          color [255, 255, 255]
          line [-255,-255, 255, 255]
          color [200, 80, 100]
          rect [0, 0, 50, 50]
          draw
          sleep 2
          c_quit
